//
//  TabBarViewController.swift
//  LinkedListTech
//
//  Created by ehsan sat on 9/20/21.
//  Copyright © 2021 ehsan sat. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
        let nvc = UINavigationController(rootViewController: vc)
        nvc.tabBarItem.image = UIImage(systemName: "text.justify")
//        nvc.tabBarController?.tabBar.backgroundColor = .black
        nvc.tabBarController?.tabBar.backgroundImage = UIImage()
        nvc.tabBarController?.tabBar.shadowImage = UIImage()
        
        
        let squareVC = storyboard.instantiateViewController(identifier: "SquareViewControllerID") as! SquareViewController
        squareVC.tabBarItem.image = UIImage(systemName: "stop")
        squareVC.tabBarController?.tabBar.backgroundColor = .black
        squareVC.tabBarController?.tabBar.backgroundImage = UIImage()
        squareVC.tabBarController?.tabBar.shadowImage = UIImage()

        
        let leftVC = storyboard.instantiateViewController(identifier: "LeftViewControllerID") as! LeftViewController
        leftVC.tabBarItem.image = UIImage(systemName: "chevron.left")
        leftVC.tabBarController?.tabBar.backgroundColor = .black
        leftVC.tabBarController?.tabBar.backgroundImage = UIImage()
        leftVC.tabBarController?.tabBar.shadowImage = UIImage()


        
        self.viewControllers = [nvc, squareVC, leftVC]

    }
    

}
