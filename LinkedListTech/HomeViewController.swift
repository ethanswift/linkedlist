//
//  HomeViewController.swift
//  LinkedListTech
//
//  Created by ehsan sat on 9/20/21.
//  Copyright © 2021 ehsan sat. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let screenWidth = UIScreen.main.bounds.width
    
    let screenHeight = UIScreen.main.bounds.height
    
    let mainTable = UITableView()
    
    var indexOfSelectedItem: Int?  {
        didSet {
            self.mainTable.reloadData()
        }
    }
    
    var isMainTableSelected: [Bool]? = [false, false, false, false, false, false, false]
    {
        didSet {
            DispatchQueue.main.async {
                self.mainTable.reloadData()
                self.view.setNeedsDisplay()
            }
        }
    }
    
    let tableData: [String] = ["Instant food, Sauces and Noodles",
                               "Stationary",
                               "Home Care",
                               "Grocery and Staples",
                               "Pet Care",
                               "Baby Care",
                               "Personal Care"]
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        setNavTabBar()
        makeViews()
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 7
    }
    
    func tableView(_ tableView: UITableView,
                   viewForHeaderInSection section: Int) -> UIView?
    {
        let view = UIView()
        view.backgroundColor = .clear
//        view.frame.size.height = 20
        return view
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if isMainTableSelected![indexPath.section] == true {
            tableView.register(ExpandedTableViewCell.self, forCellReuseIdentifier: "ExpandedCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExpandedCell") as! ExpandedTableViewCell
            cell.title.text = tableData[indexPath.section]
            cell.indexOfSelectedRow = self.indexOfSelectedItem
            cell.table.reloadData()

            return cell
        } else {
            
            tableView.register(SimpleTableViewCell.self, forCellReuseIdentifier: "SimpleCell")
            let cell = tableView.dequeueReusableCell(withIdentifier: "SimpleCell") as! SimpleTableViewCell
            cell.title.text = self.tableData[indexPath.section]

            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.isMainTableSelected![indexPath.section] == true {
            return 200
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForHeaderInSection section: Int) -> CGFloat
    {
        return 24
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath)
    {
        self.indexOfSelectedItem = indexPath.section
        self.isMainTableSelected![indexPath.section].toggle()
//        print(indexPath.section)
//        isMainTableSelected?[indexPath.section].toggle()
        for i in 0...6 {
            if i != indexPath.section {
                self.isMainTableSelected![i] = false
                DispatchQueue.main.async {
                    self.mainTable.reloadData()
                }
            }
        }
    }
    
    func setNavTabBar()
    {
        let title = UILabel()
        title.text = "ROZA.ANAA"
        title.font = .systemFont(ofSize: 18, weight: .semibold)
        title.textColor = .red
        title.backgroundColor = .white
        title.textAlignment = .center
        title.frame = CGRect(x: (screenWidth / 3) - 50, y: 0, width: 150, height: 40)
        
        let rightImgView = UIImageView()
        rightImgView.image = UIImage(systemName: "magnifyingglass")?.withRenderingMode(.alwaysTemplate)
        rightImgView.tintColor = .systemRed
        rightImgView.contentMode = .scaleAspectFit
//        rightImgView.backgroundColor = .systemPink
        rightImgView.frame = CGRect(x: (screenWidth - 85), y: 10, width: 25, height: 25)
        
        let leftImgView = UIImageView()
        leftImgView.image = UIImage(systemName: "r.circle")?.withRenderingMode(.alwaysTemplate)
        leftImgView.tintColor = .systemRed
        leftImgView.contentMode = .scaleAspectFit
//        leftImgView.backgroundColor = .systemPink
        leftImgView.frame = CGRect(x: 80, y: 10, width: 25, height: 25)

        let leftBarBtn = UIBarButtonItem(image: UIImage(systemName: "line.horizontal.3")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(leftBarBtnClicked))
        leftBarBtn.tintColor = .black
        
        let rightBarBtn = UIBarButtonItem(image: UIImage(systemName: "cart.fill.badge.plus")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(rightBarBtnClicked))
        rightBarBtn.tintColor = .systemRed

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = .white
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.systemGray2.cgColor
        self.navigationController?.navigationBar.layer.shadowRadius = 5
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.8
        self.navigationController?.navigationBar.layer.masksToBounds = false
        self.navigationController?.navigationBar.addSubview(title)
        self.navigationController?.navigationBar.addSubview(rightImgView)
        self.navigationController?.navigationBar.addSubview(leftImgView)
        self.navigationItem.leftBarButtonItem = leftBarBtn
        self.navigationItem.rightBarButtonItem = rightBarBtn
        self.tabBarController?.tabBar.barTintColor = .black
        self.tabBarController?.tabBar.isTranslucent = false

    }
    
    @objc func leftBarBtnClicked()
    {
        
    }
    
    @objc func rightBarBtnClicked()
    {
        
    }
    
    func makeViews()
    {
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
        self.view.addSubview(mainTable)
        mainTable.translatesAutoresizingMaskIntoConstraints = false
        
        layoutViews()
    }
    
    func layoutViews()
    {
        NSLayoutConstraint.activate([
            mainTable.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16),
            mainTable.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16),
            mainTable.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 100),
            mainTable.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
    

}
