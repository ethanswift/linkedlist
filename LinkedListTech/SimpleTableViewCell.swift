//
//  SimpleTableViewCell.swift
//  LinkedListTech
//
//  Created by ehsan sat on 9/20/21.
//  Copyright © 2021 ehsan sat. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {
    
    let title = UILabel()
    
    let arrow = UIImageView()
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        makeViews()
//        self.backgroundColor = .systemBlue
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        makeViews()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func makeViews() {
        title.text = " "
        title.textAlignment = .left
        title.textColor = .red
        title.font = .systemFont(ofSize: 18, weight: .semibold)
        self.contentView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        
        arrow.image = UIImage(systemName: "chevron.up")?.withRenderingMode(.alwaysTemplate)
        arrow.tintColor = .black
        arrow.contentMode = .scaleAspectFit
        self.contentView.addSubview(arrow)
//        self.addSubview(arrow)
        arrow.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.borderColor = UIColor.systemGray4.cgColor
        self.contentView.layer.borderWidth = 3
        
        layoutViews()
    }
    
    func layoutViews() {
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 40),
            title.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            title.widthAnchor.constraint(equalToConstant: 300),
            title.heightAnchor.constraint(equalToConstant: 50)
            
        ])
        
        NSLayoutConstraint.activate([
            arrow.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
            arrow.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10),
            arrow.widthAnchor.constraint(equalToConstant: 30),
            arrow.heightAnchor.constraint(equalToConstant: 30)
            
        ])

        
    }

}
