//
//  ExpandedTableViewCell.swift
//  LinkedListTech
//
//  Created by ehsan sat on 9/20/21.
//  Copyright © 2021 ehsan sat. All rights reserved.
//

import UIKit

class ExpandedTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    let title = UILabel()
    
    let arrow = UIImageView()
    
    let line = UIView()
    
    let table = UITableView()
    
    let screenWidth = UIScreen.main.bounds.width
    
//    var tableData: [String]? = []
//    {
//        didSet
//        {
//
//        }
//    }
    
    var indexOfSelectedRow: Int? = 0
    
//    var numberOfRows: Int? = 1
//    {
//        didSet
//        {
//        }
//    }
    
    var rowsDataArray: [String]? = []

    var firstRowArray: [String]? = ["Jams, Honey & Spreads", "Pickles & Chutneys", "Readymade & Mixes"]
        
    var thirdRowArray: [String]? = ["Edible Oils", "Ghee & Vanaspati", "Dry Fruits & Nuts"]
    
    override init(style: UITableViewCell.CellStyle,
                  reuseIdentifier: String?)
    {
        super.init(style: style,
                   reuseIdentifier: reuseIdentifier)
        
        makeViews()
    }
    
    required init?(coder: NSCoder)
    {
        super.init(coder: coder)
        
        makeViews()
    }

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int
    {
        if indexOfSelectedRow == 0 {
            return 3
        } else if indexOfSelectedRow == 1 {
            return 2
        } else if indexOfSelectedRow == 2 {
            return 3
        } else if indexOfSelectedRow == 3 {
            return 2
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if indexOfSelectedRow == 0 {
            cell?.textLabel?.text = firstRowArray![indexPath.row]
        } else if indexOfSelectedRow == 1 {
            cell?.textLabel?.text = ["1", "2", "3"][indexPath.row]
        } else if indexOfSelectedRow == 2 {
            cell?.textLabel?.text = thirdRowArray![indexPath.row]
        } else if indexOfSelectedRow == 3 {
            cell?.textLabel?.text = ["1", "2", "3"][indexPath.row]
        } else {
            cell?.textLabel?.text = ["1", "2", "3"][indexPath.row]
        }
//        cell?.textLabel?.text = rowsDataArray?[indexPath.row] ?? "No Data"
        cell?.textLabel?.textAlignment = .center
        cell?.textLabel?.textColor = .black
        cell?.textLabel?.font = .systemFont(ofSize: 16,
                                            weight: .semibold)
        return cell!
    }
    
    func makeViews () {
        
        title.text = "another thing"
        title.textAlignment = .left
        title.textColor = .red
        title.font = .systemFont(ofSize: 16,
                                 weight: .semibold)
        self.contentView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        
        arrow.image = UIImage(systemName: "chevron.down")?.withRenderingMode(.alwaysTemplate)
        arrow.tintColor = .black
        arrow.contentMode = .scaleAspectFit
        self.contentView.addSubview(arrow)
        arrow.translatesAutoresizingMaskIntoConstraints = false
        
        line.backgroundColor = .systemGray4
        line.layer.zPosition = 3
        self.contentView.addSubview(line)
        line.translatesAutoresizingMaskIntoConstraints = false
        
        table.delegate = self
        table.dataSource = self
        table.register(UITableViewCell.self,
                       forCellReuseIdentifier: "Cell")
        table.separatorStyle = .none
//        table.backgroundColor = .systemGray6
        self.contentView.addSubview(table)
        table.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.borderColor = UIColor.systemGray4.cgColor
        self.contentView.layer.borderWidth = 3
        
        layoutViews()
    }
    
    func layoutViews () {
        
        NSLayoutConstraint.activate([
            title.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 30),
            title.widthAnchor.constraint(equalToConstant: 300),
            title.heightAnchor.constraint(equalToConstant: 50),
            title.topAnchor.constraint(equalTo: self.contentView.topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            arrow.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -24),
            arrow.widthAnchor.constraint(equalToConstant: 30),
            arrow.heightAnchor.constraint(equalToConstant: 30),
            arrow.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16)
        ])
        
        NSLayoutConstraint.activate([
            line.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor),
            line.widthAnchor.constraint(equalToConstant: 300),
            line.heightAnchor.constraint(equalToConstant: 1),
            line.topAnchor.constraint(equalTo: title.bottomAnchor)
        ])

        
        NSLayoutConstraint.activate([
            table.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            table.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
//            table.widthAnchor.constraint(equalTo: self.contentView.widthAnchor),
            table.heightAnchor.constraint(equalToConstant: 150),
            table.topAnchor.constraint(equalTo: title.bottomAnchor)
        ])


    }


}
